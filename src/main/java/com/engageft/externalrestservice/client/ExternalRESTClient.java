package com.engageft.externalrestservice.client;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

/**
 * This class is common class for calling external REST APIs
 * 
 * @author amcdonald
 *
 */
public class ExternalRESTClient {

	private final static Logger LOG = LoggerFactory.getLogger(ExternalRESTClient.class);

	RestTemplate restTemplate;

	public ExternalRESTClient(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public ExternalRESTClient() {
	}
		
   public void setInterceptor(ClientHttpRequestInterceptor interceptor) {
	     restTemplate.getInterceptors().add(interceptor);
   }
	
	public <T> ResponseEntity<T> requestToExternalServer(String uri, HttpMethod method, Object reqObj, Class<T> responseType, Map<String, String> uriVariables) {
		return requestToExternalServer(uri, method, reqObj, responseType, null, uriVariables);
	}
	
	public <T> ResponseEntity<T> requestToExternalServer(String uri, HttpMethod method, Object reqObj, Class<T> responseType) {
		return requestToExternalServer(uri, method, reqObj, responseType, null, null);
	}
	
	public <T> ResponseEntity<T> requestToExternalServer(String uri, HttpMethod method, Object reqObj, Class<T> responseType, HttpHeaders headers) {
		return requestToExternalServer(uri, method, reqObj, responseType, headers, null);
	}

	public <T>  ResponseEntity<T> requestToExternalServer(String uri, HttpMethod method, Object reqObj, Class<T> responseType, HttpHeaders headers, Map<String, String> uriVariables) {
		ResponseEntity<T> response = null;
		if(headers == null) {
			headers = new HttpHeaders();
		}
		// set default content type to be JSON
		if(headers.getContentType() == null) {
			headers.setContentType(MediaType.APPLICATION_JSON);
		}
		if(uriVariables == null ){
			uriVariables = new HashMap<String, String>();
		}
		// 
		// LOG input data
		//
		LOG.info("URI: " + uri + " Method: " + method);
		if(reqObj != null) {
			LOG.debug("Request class: " + reqObj.getClass().getName() + " Request object: " + reqObj.toString());
		
		}
		logMap("Headers: ",  headers.toSingleValueMap());
		logMap("Parameters: ", uriVariables);
		//
		//  Invoke REST call
		//
		try {
			response = restTemplate.exchange(uri, method, new HttpEntity<Object>(reqObj, headers), responseType, uriVariables);
		} catch (Exception e) {
			// Client errors will be handled in the CustomErrorHandler injected into the RestTemplate class
			// This exception block is a stop gap
			throw new RuntimeException(e);
		} 
		LOG.info("Response code:  " +  response.getStatusCode().value());
		return response;
	}
	
	private void logMap(String label, Map<String, String> map) {
		map.forEach( (k, v)-> LOG.debug(label +  " Name : " + k + " Value: "  + v));
	}
}
