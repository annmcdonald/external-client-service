package com.engageft.externalrestservice.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.engageft.externalrestservice.payvox.service.request.model.constants.Role;
import com.engageft.externalrestservice.payvox.service.request.model.constants.RoleType;

/**
 * @author A.McDonald 
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RoleValidator.class)
public @interface ValidateRole {
	String message() default "{com.xxx.yyy.ValidateRole.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    RoleType[] value() default {};
}
