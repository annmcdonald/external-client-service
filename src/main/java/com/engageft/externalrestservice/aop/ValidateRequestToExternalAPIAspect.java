package com.engageft.externalrestservice.aop;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.engageft.externalrestservice.constants.IErrorCode;
import com.engageft.externalrestservice.exception.ExternalRESTServiceException;

/**
 * @author A. McDonald
 */

@Aspect
@Component
public class ValidateRequestToExternalAPIAspect {
	
   private static final Logger LOG = LoggerFactory.getLogger(ValidateRequestToExternalAPIAspect.class);

	
	@Autowired
    Validator externalRESTRequestValidator;

@Pointcut(value = "@annotation(com.engageft.externalrestservice.aop.ValidateRequestToExternalAPI) && args(request,..)", argNames = "request")
	public void validateRequest(Object request) {
	}
	
@Before("validateRequest(request)")
	public void validate(Object request) throws Exception {
		Set<ConstraintViolation<Object>> violations = externalRESTRequestValidator.validate(request);
      	if(violations != null && violations.size() > 0) {
      		String returnMsg = null;
      		for (ConstraintViolation<Object> violation : violations) {
      			LOG.debug("violation: " + violation);
      			if(returnMsg == null) {
      				returnMsg = violation.getRootBeanClass().getSimpleName() + ": ";
      			} else {
      				returnMsg += " ";
      			}
      			if(StringUtils.isEmpty(violation.getMessageTemplate())) {
      				returnMsg += violation.getPropertyPath().toString() + " is invalid."; 
      			}else {
      				returnMsg += violation.getMessageTemplate();
      			}
      		}
      		throw new ExternalRESTServiceException( IErrorCode.PAYVOX_REQUEST_INVALID_8001, returnMsg, HttpStatus.BAD_REQUEST);
      	}else {
      		LOG.debug(request.getClass().getName() + " is valid.");
      	}
	}
}
