package com.engageft.externalrestservice.aop;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.engageft.externalrestservice.payvox.service.request.model.constants.Role;
import com.engageft.externalrestservice.payvox.service.request.model.constants.RoleType;

public class RoleValidator implements ConstraintValidator<ValidateRole, Enum<Role>> {
   private RoleType[] allowedTypes;
   
   @Override
   public void initialize(ValidateRole constraint) {
       allowedTypes =  constraint.value();
   }
   
   @Override
public boolean isValid(Enum<Role> value, ConstraintValidatorContext arg1) {
	   if (value == null) return false;
	   for (RoleType  type: allowedTypes) {
		   switch (type) {
		   		case CUSTOMER:
		   			if(isCustomer(value)) {
		   				return true;
		   			}
		   			break;
		   		case ADMIN:
		   			if(isAdmin(value)) {
		   				return true;
		   			}
		   }
	   }
	return false;
}
   private boolean isCustomer(Enum<Role> value) {
       return equalsRole(value, Role.CARDHOLDER) || equalsRole(value, Role.CSR);
   }

   private boolean isAdmin(Enum<Role> value) {
       return equalsRole(value, Role.CSR) || equalsRole(value, Role.ADMIN) || equalsRole(value, Role.ACCOUNTHOLDER);
   }

   private boolean equalsRole(Enum<Role> value1, Enum<Role> value2) {
	    return value1.equals(value2);
   }



    
} 