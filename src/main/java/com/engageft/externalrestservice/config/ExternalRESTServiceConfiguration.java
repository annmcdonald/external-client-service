package com.engageft.externalrestservice.config;

import java.nio.charset.Charset;
import java.util.Arrays;

import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.engageft.externalrestservice.client.ExternalRESTClient;
import com.engageft.externalrestservice.exception.handler.CustomResponseErrorHandler;
import com.engageft.externalrestservice.exception.handler.RestResponseEntityExceptionHandler;
import com.engageft.externalrestservice.payvox.service.cache.BillerCache;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class will be used for all external REST service configuration.
 * 
 * @author amcdonald
 * 
 */

@Configuration
@EnableAspectJAutoProxy
@PropertySource({ "classpath:externalREST.properties" })
@ComponentScan( "com.engageft.externalrestservice"  ) 
// @PropertySource({"file:${catalina.home}/conf/aws.properties"})
public class ExternalRESTServiceConfiguration {

	private final static Logger LOG = LoggerFactory.getLogger(ExternalRESTServiceConfiguration.class);

	 @Value("${mpgs.basicauth.username}")
	  private String username;

	 @Value("${mpgs.basicauth.password}")
	 private String password;
	 
	 private @Value("${connecttimeout}") String httpConnectTimeout  ;
	 private @Value("${readtimeout}") String httpReadTimeout;
	 private @Value("${connectionpool}") String httpConnectionPool  ;
	 private @Value("${connectionperroute}") String httpConnectionPerRoute;
	 private @Value("${ispooling") String isHttpConnectionPooling;
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		PropertySourcesPlaceholderConfigurer ppc = new PropertySourcesPlaceholderConfigurer();
		ppc.setIgnoreResourceNotFound(true);
		ppc.setIgnoreUnresolvablePlaceholders(true);
		return ppc;
	}
	
	@Bean
	public MessageSource messageSource() {
		String[] baseNames = { "/errorproperties/error" };
		ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
		resourceBundleMessageSource.setBasenames(baseNames);
		resourceBundleMessageSource.setDefaultEncoding("UTF-8");

		return resourceBundleMessageSource;
	}

	/**
	 * This bean will be used as REST client for external REST services
	 */
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
		//
		// For now just using String.class and converting to POJO in the service method in order to 
		// handle errors such as a redirect 
		//
		//restTemplate.getMessageConverters().add(mappingJacksonHttpMessageConverter());
		//restTemplate.getMessageConverters().add(0, stringHttpMessageConverter());
		restTemplate.setErrorHandler(restTemplateError());
		return restTemplate;
	}

	@Bean
	public StringHttpMessageConverter stringHttpMessageConverter() {
		StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
		converter.setSupportedMediaTypes(Arrays.asList(new MediaType("text", "html", Charset.forName("UTF-8"))));
		return converter;
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setObjectMapper(mapper());
		return converter;
	}

	/**
	 * This bean is used to have single point to call the external REST apis.
	 */
	@Bean
	public ExternalRESTClient externalRESTClient() {
		return new ExternalRESTClient(restTemplate());
	}

	/**
	 * This bean will be used as setting configuration for HTTP request to external
	 * services.
	 */
	@Bean
	public HttpComponentsClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory httpClientFactory;
		if(Boolean.getBoolean(this.isHttpConnectionPooling)) {
			httpClientFactory = new HttpComponentsClientHttpRequestFactory(httpClient());
		}else {
			httpClientFactory = new HttpComponentsClientHttpRequestFactory();
		}
		 httpClientFactory.setConnectTimeout(new Integer(this.httpConnectTimeout));
		 httpClientFactory.setReadTimeout(new Integer(this.httpReadTimeout));
		return httpClientFactory;
	}
	
	
	@Bean
	public HttpClient httpClient() {
		
		CloseableHttpClient httpClient = null;
		try {
//			SchemeRegistry schemeRegistry = new SchemeRegistry();
//			schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
//			SSLContext sslContext = SSLContext.getInstance("SSL");
//	        sslContext.init(null, null, null);
//			schemeRegistry.register(new Scheme("https", 443, new SSLSocketFactory( sslContext, new AllowAllHostnameVerifier())));
//			PoolingClientConnectionManager connectionManager = new PoolingClientConnectionManager(schemeRegistry);
			
			PoolingHttpClientConnectionManager connectionManager 
			  = new PoolingHttpClientConnectionManager();
			
			Integer connectionPool = Integer.parseInt(this.httpConnectionPool);
			Integer connectionPerRoute = Integer.parseInt(this.httpConnectionPerRoute);
			connectionManager.setMaxTotal(connectionPool);
			connectionManager.setDefaultMaxPerRoute(connectionPerRoute);
			
			ConnectionKeepAliveStrategy keepAliveStrategy = new ConnectionKeepAliveStrategy() {
			    @Override
			    public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
			        HeaderElementIterator it = new BasicHeaderElementIterator
			            (response.headerIterator(HTTP.CONN_KEEP_ALIVE));
			        while (it.hasNext()) {
			            HeaderElement he = it.nextElement();
			            String param = he.getName();
			            String value = he.getValue();
			            if (value != null && param.equalsIgnoreCase
			               ("timeout")) {
			                return Long.parseLong(value) * 1000;
			            }
			        }
			        return 5 * 1000;
			    }
			
			};
			
			httpClient = HttpClients.custom()
					  .setKeepAliveStrategy(keepAliveStrategy)
					  .setConnectionManager(connectionManager)
					  .build();
			
		} catch(Exception e) {
			LOG.error("failed to create the connection: " + e.getMessage());
			e.printStackTrace();
		}
		return httpClient;
	}	


	/**
	 * This bean is used to handle all the errors coming from RestTemplate call.
	 */
	@Bean
	 public CustomResponseErrorHandler restTemplateError() {
		return new CustomResponseErrorHandler();
	 }

	/**
	 * This bean will handle all the exceptions defined in it and will send
	 * appropriate response to UI.
	 */
	 @Bean
	 public RestResponseEntityExceptionHandler exceptionHandler() {
	 return new RestResponseEntityExceptionHandler();
	 }

	@Bean
	public com.fasterxml.jackson.databind.ObjectMapper mapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		return mapper;
	}
	
	@Bean
	public Validator externalRESTRequestValidator() {
    	return Validation.buildDefaultValidatorFactory().getValidator();
	}
	
	@Bean
	public BillerCache billerCache(){
		return new BillerCache();
	}
	
}
