package com.engageft.externalrestservice.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class is common class for REST API error response in JSON format
 * @author AMcDonaldl
 *
 */
@JsonInclude(Include.NON_NULL)
public class RestErrorMessage {

	
    private Integer error;
    private String message;
    private String reason;
    private List<String> errors = new ArrayList<String>();
    
    public RestErrorMessage() {
		
	}
 
    public RestErrorMessage(Integer error, String message, String reason, List<String> errors) {
    	this.error = error;
    	this.errors = errors;
        this.message = message;
        this.reason = reason;
        
    }

	public Integer getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}

	public List<String> getErrors() {
		return errors;
	}

	public String getReason() {
		return reason;
	}
	
}
