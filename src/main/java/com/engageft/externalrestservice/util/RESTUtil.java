package com.engageft.externalrestservice.util;

import org.springframework.http.HttpStatus;

public class RESTUtil {

    public static boolean isError(HttpStatus status) {
//        HttpStatus.Series series = status.series();
//        return (HttpStatus.Series.CLIENT_ERROR.equals(series)
//                || HttpStatus.Series.SERVER_ERROR.equals(series));
    	return (!status.equals(HttpStatus.OK));
    }
}