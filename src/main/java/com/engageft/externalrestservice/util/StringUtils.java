package com.engageft.externalrestservice.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class StringUtils {
	
	public static String blockCharacters(String sourceStr, int lastVisible, char replaceChar) {
		String s = "";
		int length = sourceStr.length();
		int index = length - lastVisible;
		for (int i=0;i<length;i++) {
			if(i<index) {
				s += replaceChar; 
			}else {
				s += sourceStr.charAt(i);
			}
		}	
		return s;
	}

	public static void main(String[] args) {
		Integer startResult = new Integer(100);
		Integer maxResults = new Integer(200);
		String fruit = "apple";
		String vegi = "onion";
		String s = "BillerDirectoryCategory: [\n";
		String tmpString = (startResult==null)?"null":startResult.toString();
		s += "startResult: " + tmpString + "\n";
		tmpString = (maxResults==null)?"null":maxResults.toString();
		s += "maxResult: " + tmpString + "\n";
		s += "Subobject: [\n";
		s += "fruit: " + fruit + "\n";
		s += "vegi: " + vegi + "\n";
		s += "]\n";
		s += "]\n";
		String outputStr = StringUtils.indentToStringMultilineObjects(s);
		System.out.println(outputStr);
	}
	
	public static String outputMapforToString( Map<String, Object> map) {
		 String result = map.entrySet()
		            .stream()
		            .map(entry -> "key: " + entry.getKey() + "  value: " + entry.getValue().toString())
		            .collect(Collectors.joining(", "));
		return result;
	}
	
	public static String indentToStringMultilineObjects(String sourceStr) {
		String returnStr = null;
			List<String> sList = Stream.of(sourceStr.split("\n"))
			.map(elem -> new String(elem + "\n"))
			.collect(Collectors.toList());
			int tabCount = 0;
			for(String s: sList ) {
				if(returnStr == null) {
					returnStr = s;
				}else {
					for(int i=0;i<tabCount;i++) {
						returnStr += "\t";
					}
					returnStr+= s;
					if(s.contains("[")) tabCount++;
					if(tabCount >0 && s.contains("]")) tabCount--;
				}
			}
		return returnStr;
	}


}
