package com.engageft.externalrestservice.util;


import java.io.IOException;

import com.engageft.externalrestservice.payvox.service.request.model.FundingAccount;
import com.engageft.externalrestservice.payvox.service.request.ProcessProfileRequest;
import com.engageft.externalrestservice.payvox.service.request.model.constants.Role;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class MarshallingUtil {

	private static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		return mapper;
	}

	public static <T> String marshalToJson( T o) throws JsonProcessingException {
		return getObjectMapper().writeValueAsString(o);
	}
	
	public static <T> T convertJsonToPOJO(String s, Class<T> target) throws JsonParseException, JsonMappingException, IOException {
		return  getObjectMapper().readValue(s, target);
}

	public static void main(String[] args) {
	       ProcessProfileRequest profile = new ProcessProfileRequest();
	       FundingAccount fundingAccount = new FundingAccount();
	       profile.setRole(Role.ADMIN);
	       fundingAccount.setNumber("123456");
	       fundingAccount.setName("name");
	       profile.setEmail("ann.mcdonald@engageft.com");
	       profile.setFname("Ann");
	       profile.setLname("McDonald");
	       profile.setPhone("7147133024");;
	       profile.setProgramId("programId");;
	       profile.setUserId("userId");;
	       profile.setLocale("EN");
	       profile.setFundingAccount(fundingAccount);
	       String s = null;
		try {
			s = MarshallingUtil.marshalToJson(profile);
			if(s.indexOf("ADMIN" ) > 0) {
				int i = s.indexOf("ADMIN");
				String s_new = s.substring(0, i);
				s_new += s.substring(i+2);
				System.out.println(s_new);
				profile = MarshallingUtil.convertJsonToPOJO(s_new, ProcessProfileRequest.class);
				System.out.println("Profile: " + profile.toString());
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       System.out.println(s);
	}
}
