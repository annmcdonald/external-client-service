package com.engageft.externalrestservice.exception.handler;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import com.engageft.externalrestservice.util.RESTUtil;


/**
 * This class handles the Exception raised by RESTTemplate.
 * @author amcdonald 
 *
 */

public class CustomResponseErrorHandler implements ResponseErrorHandler {

	private static Logger LOG = LoggerFactory.getLogger(CustomResponseErrorHandler.class);
	
	@Override
    public void handleError(ClientHttpResponse response) throws IOException {
        LOG.error("Response error within handleError: {} {}", response.getStatusCode(), response.getStatusText());
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return RESTUtil.isError(response.getStatusCode());
    }
	
}
