package com.engageft.externalrestservice.exception.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.engageft.externalrestservice.exception.ExternalRESTServiceException;
import com.engageft.externalrestservice.model.Resource;
import com.engageft.externalrestservice.model.RestErrorMessage;

/**
 * This is centralized class for handling all the exceptions.
 * @author A. McDonald 
 *
 */

@ControllerAdvice
@Component
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	private final static Logger LOG = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);
	
	@Autowired
	MessageSource messageSource;
	
	public RestResponseEntityExceptionHandler() {
		super();
	}
    
    @ExceptionHandler(value = ExternalRESTServiceException.class)
    protected ResponseEntity<Object> externaRESTException(ExternalRESTServiceException exception) {
    	return createErrorResponse(exception.getCode(), exception.getMessage(), null, null, exception.getHttpStatus(),exception.getObj());        
    }
    
	 /**
	  * Common method to generate error response to REST.
	  * @param error
	  * @param message
	  * @param errors
	  * @param headers
	  * @param status
	  * @return
	  */
	 public ResponseEntity<Object> createErrorResponse(Integer code, String message, List<String> errors, HttpHeaders headers, HttpStatus status){
		 String localizedErrorMsg = getLocalizedErrorMessage(code,message);
		 RestErrorMessage errorMessage = new RestErrorMessage(code, localizedErrorMsg, message, errors);
		 Resource<RestErrorMessage> resource = new Resource<RestErrorMessage>(false, errorMessage);
		 return new ResponseEntity<Object>(resource, headers, status);
	 }
	 
	 /**
	  * Common method to generate error response to REST.
	  * @param error
	  * @param message
	  * @param errors
	  * @param headers
	  * @param status
	  * @param params
	  * @return
	  */
	 public ResponseEntity<Object> createErrorResponse(Integer code, String message, List<String> errors, HttpHeaders headers, HttpStatus status, Object[] params){
		 String localizedErrorMsg = getLocalizedErrorMessage(code,message,params);
		 RestErrorMessage errorMessage = new RestErrorMessage(code, localizedErrorMsg, message, errors);
		 Resource<RestErrorMessage> resource = new Resource<RestErrorMessage>(false, errorMessage);
		 return new ResponseEntity<Object>(resource, headers, status);
	 }
	 
	 /**
	  * This method will return the localized message depending on the locale.
	  * @param errorCode
	  * @return
	  */
	 public String getLocalizedErrorMessage(Integer errorCode, String errorMessage, Object[] params){
		 String msg = errorMessage;		 
		 try {
			 msg = messageSource.getMessage(errorCode.toString()     , params, LocaleContextHolder.getLocale());
		 }catch(Exception e){
			 LOG.error("Error While Finding Localization error message for Code : " + errorCode + "\n Exception is: "+ e);
		 }
		 return msg;
	 }
	 
	 /**
	  * This method will return the localized message depending on the locale.
	  * @param errorCode
	  * @return
	  */
	 public String getLocalizedErrorMessage(Integer errorCode, String errorMessage){
		 String msg = errorMessage;		 
		 try {
			 msg = messageSource.getMessage(errorCode.toString() , null, LocaleContextHolder.getLocale());
		 }catch(Exception e){
			 LOG.error("Error While Finding Localization error message for Code : " + errorCode + "\n Exception is: "+ e);
		 }
		 return msg;
	 }
	 
}
