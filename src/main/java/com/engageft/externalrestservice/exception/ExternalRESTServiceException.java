package com.engageft.externalrestservice.exception;

import org.springframework.http.HttpStatus;

public class ExternalRESTServiceException extends RuntimeException  {

	private Integer code;
	private HttpStatus httpStatus;
	private Object[] obj;
	
   public ExternalRESTServiceException() {
	   super();
   }
	
	public ExternalRESTServiceException(Integer code, String msg) {
		super(msg);
		this.code = code;
	}
	
	public ExternalRESTServiceException(Integer code, String msg, HttpStatus httpStatus) {
		super(msg);
		this.code = code;
		this.httpStatus = httpStatus;
	}
	
	public ExternalRESTServiceException(Integer code, Object obj[], String msg) {
		super(msg);
		this.code = code;
		this.obj = obj;
	}

	public ExternalRESTServiceException(Integer code, Object obj[], String msg, HttpStatus httpStatus) {
		super(msg);
		this.code = code;
		this.obj = obj;
		this.httpStatus = httpStatus;
	}
	
	public Integer getCode() {
		return code;
	}

	public Object[] getObj() {
		return obj;
	}

	public HttpStatus getHttpStatus() {
		if(httpStatus == null){
			return HttpStatus.OK;
		}
		return httpStatus;
	}
	
}
