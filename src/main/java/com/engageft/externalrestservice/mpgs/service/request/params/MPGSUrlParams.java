package com.engageft.externalrestservice.mpgs.service.request.params;

import java.util.HashMap;
import java.util.Map;

public class MPGSUrlParams {
	private Map<String, String> paramMap = new HashMap<String, String>();
    private String merchantId;

    public MPGSUrlParams() {
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
        paramMap.put("merchantId", merchantId);
    }

    public Map<String, String> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<String, String> paramMap) {
		this.paramMap = paramMap;
	}

	public boolean validate() {
    	if(merchantId != null) {
    		return true;
    	}else
    		return false;
    }
}
