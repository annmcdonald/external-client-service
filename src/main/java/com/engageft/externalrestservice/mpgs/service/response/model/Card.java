package com.engageft.externalrestservice.mpgs.service.response.model;

public class Card {
    private String number;
    private String nameOnCard;
    private Expiry expiry;
    private Pin Pin;
    private String accountType;
    private String brand;
    private Expiry deviceSpecificExpiry;
    private String deviceSpecificNumber;
    private String emvRequest;
    private String emvResponse;
    private String encryption;
    private String fundingMethod;
    private String issuer;
    private String localBrand;
    private MobileWallet mobileWallet;
    private String scheme;
    private String sequenceNumber;
    private String tags;
    private String trackDataProvided;
//    private P2pe p2pe;
}
