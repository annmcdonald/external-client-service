package com.engageft.externalrestservice.mpgs.service.request.params;

public class TokenizationUrlParams extends MPGSUrlParams {
    private String tokenId;

    public TokenizationUrlParams() {
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
        super.getParamMap().put("tokenId", tokenId);
    }

    @Override
    public boolean validate() {
    	if(tokenId != null && super.validate()) {
    		return true;
    	}else 
    		return false;
    }
}
