package com.engageft.externalrestservice.mpgs.service;


import com.engageft.externalrestservice.client.ExternalRESTClient;
import com.engageft.externalrestservice.mpgs.service.request.CreateUpdatePaymentRequest;
import com.engageft.externalrestservice.mpgs.service.request.params.MPGSUrlParams;
import com.engageft.externalrestservice.mpgs.service.request.params.TokenizationUrlParams;
import com.engageft.externalrestservice.mpgs.service.response.CreateUpdatePaymentResponse;
import com.engageft.externalrestservice.mpgs.service.response.InformationResponse;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

/**
 *
 * The REST client to access External REST services.
 *
 */
/**
 * @author annmcdonald
 *
 */
@Service
public class MPGSService {
    private static final Logger LOG = LoggerFactory.getLogger(MPGSService.class);
    
    @Autowired
    ExternalRESTClient externalRESTClient;
    
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private @Value("${mpgs.protocol}") String protocol;
    private @Value("${mpgs.host}") String domain;
    private @Value("${mpgs.path}") String path;
    private @Value("${mpgs.basicauth.username}") String username;
    private @Value("${mpgs.basicauth.password") String password;
    private @Value("${mpgs.information.resource}") String informationResource;
    private @Value("${mpgs.payment.resource}") String paymentResource;
    private @Value("${mpgs.update.payment.resource}") String updatePaymentResource;
    
    /**
     * Request to check that the gateway is operating.
     * 
     * @return
     */
    public InformationResponse getInformation() {
    	ResponseEntity<InformationResponse> responseEntity = null;
    	String url = protocol + domain + path + informationResource;
    	LOG.debug("url: " + url);
    	try {
    		responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.GET, null, InformationResponse.class);
    	}catch (Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    	return  (InformationResponse) responseEntity.getBody();
    }
    
    /**
     * Request for the gateway to store payment instrument (e.g. credit or debit cards, gift cards, 
     * ACH bank account details) against a token, where the system generates the token id.* 
     * 
     * @param request
     * @param params
     * @return
     */
    public  CreateUpdatePaymentResponse storePayment(CreateUpdatePaymentRequest request, MPGSUrlParams params) {
     	ResponseEntity<CreateUpdatePaymentResponse> responseEntity = null;
    	String url = protocol + domain + path + paymentResource;
    	LOG.debug("url: " + url);
    	//
    	// Validation of parameters
    	//
    	if(!params.validate()) {
    		// Add validation error handling
    		return null;
    	}
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		headers.add("Authorization", "Basic " + token);
		//
    	try {
    		responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, CreateUpdatePaymentResponse.class, headers, params.getParamMap());
    	}catch (Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    	return  (CreateUpdatePaymentResponse) responseEntity.getBody();
    }
    
    
    /**
     * 
     * Request for the gateway to store payment instrument (e.g. credit or debit cards, gift cards, 
     * ACH bank account details) against a token, where you provide the token id.
     * 
     * @param request
     * @param params
     * @return
     */
    public  CreateUpdatePaymentResponse updatePayment(CreateUpdatePaymentRequest request, TokenizationUrlParams params) {
      	ResponseEntity<CreateUpdatePaymentResponse> responseEntity = null;
    	String url = protocol + domain + path + updatePaymentResource;
    	LOG.debug("url: " + url);
    	//
    	//  Validation of parameters
    	//
    	if(!params.validate()) {
    		// Add validation error handling
    		return null;
    	}
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		headers.add("Authorization", "Basic " + token);
		//
    	try {
    		responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.PUT, request, CreateUpdatePaymentResponse.class, headers, params.getParamMap());
    	}catch (Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    	return  (CreateUpdatePaymentResponse) responseEntity.getBody();
    }
    
}
