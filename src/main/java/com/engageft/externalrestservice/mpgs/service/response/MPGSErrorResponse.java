package com.engageft.externalrestservice.mpgs.service.response;

import com.engageft.externalrestservice.mpgs.service.response.constants.ErrorCause;
import com.engageft.externalrestservice.mpgs.service.response.constants.ErrorValidationType;

/**
 * Information on possible error conditions that may occur while processing an operation using the API.
 */
public class MPGSErrorResponse {

    //Broadly categorizes the cause of the error.
    private ErrorCause cause;//  Enumeration

    // Textual description of the error based on the cause.
    private String explanation;

    // Indicates the name of the field that failed validation.
    private String field;

    // Indicates the code that helps the support team to quickly identify
    // the exact cause of the error.
    private String supportCode;

    // Indicates the type of field validation
    private ErrorValidationType validationType;//  Enumeration

    public MPGSErrorResponse() {
    }

    public ErrorCause getCause() {
        return cause;
    }

    public String getExplanation() {
        return explanation;
    }

    public String getField() {
        return field;
    }

    public String getSupportCode() {
        return supportCode;
    }

    public ErrorValidationType getValidationType() {
        return validationType;
    }
}
