package com.engageft.externalrestservice.mpgs.service.response.constants;

/**
 * Account Updater response status.
 * A token is marked as invalid, if an Account Updater response indicates, that the card details
 * stored against the token are invalid
 */
public enum MPGSStatus {

    // The card details stored against the token have been identified by Account Updater to be invalid.
    // Transaction requests using this token will be rejected by the gateway.
    INVALID,

    //The card details stored against the token are valid, i.e. if the token is used when submitting
    // a transaction request, the request will be submitted to the acquirer for processing.
    VALID
}
