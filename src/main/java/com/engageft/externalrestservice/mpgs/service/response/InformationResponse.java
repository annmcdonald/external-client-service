package com.engageft.externalrestservice.mpgs.service.response;

public class InformationResponse {
    private String gatewayVersion;
    private String status;

    public InformationResponse() {
    }

    public String getGatewayVersion() {
        return gatewayVersion;
    }

    public void setGatewayVersion(String gatewayVersion) {
        this.gatewayVersion = gatewayVersion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "InformationResponse{" +
                "gatewayVersion='" + gatewayVersion + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
