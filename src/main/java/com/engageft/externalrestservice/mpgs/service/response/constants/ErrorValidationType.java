package com.engageft.externalrestservice.mpgs.service.response.constants;

public enum ErrorValidationType {
    //  The request contained a field with a value that did not pass validation.
    INVALID,

    // The request was missing a mandatory field.
    MISSING,

    //  The request contained a field that is unsupported.
    UNSUPPORTED
}
