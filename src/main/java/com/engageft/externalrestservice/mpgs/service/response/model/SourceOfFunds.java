package com.engageft.externalrestservice.mpgs.service.response.model;

/**
 * Information about the payment type selected by the payer for this payment and the source of the funds.
 * Depending on the payment type the source of the funds can be a debit or credit card,
 * bank account, or account with a browser payment provider (such as PayPal)
 *
 * For card payments the source of funds information may be represented by combining one or more
 * of the following: explicitly provided card details, a session identifier which the gateway will
 * use to look up the card details and/or a card token.
 *
 * Precedence rules will be applied in that explicitly provided card details will
 * override session card details which will override card token details.
 */
public class SourceOfFunds {
    private String token;
    private String type;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
