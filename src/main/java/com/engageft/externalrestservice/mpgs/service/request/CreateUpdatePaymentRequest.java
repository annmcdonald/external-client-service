package com.engageft.externalrestservice.mpgs.service.request;

import com.engageft.externalrestservice.mpgs.service.response.model.SourceOfFunds;

/**
 * Request for the gateway to store or update a payment instrument.
 */
public class CreateUpdatePaymentRequest  {
    private String correlationId;
    private SourceOfFunds sourceOfFunds;
    private String verificationStrategy;

    public CreateUpdatePaymentRequest() {
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public SourceOfFunds getSourceOfFunds() {
        return sourceOfFunds;
    }

    public void setSourceOfFunds(SourceOfFunds sourceOfFunds) {
        this.sourceOfFunds = sourceOfFunds;
    }

    public String getVerificationStrategy() {
        return verificationStrategy;
    }

    public void setVerificationStrategy(String verificationStrategy) {
        this.verificationStrategy = verificationStrategy;
    }
}
