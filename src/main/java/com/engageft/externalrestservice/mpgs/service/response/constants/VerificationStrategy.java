package com.engageft.externalrestservice.mpgs.service.response.constants;

/**
 * The strategy used to verify the card details before they are stored.
 */
public enum VerificationStrategy {

    /**
     *
     * The gateway verifies the card details by performing an authorization for a nominal amount
     * (for example, $1.00).
     * The card details are sent to the acquirer for verification.
     * This method requires your merchant profile to be configured with ""Auth Then Capture"" transaction mode.
     */
    ACQUIRER,

    /**
     * The gateway validates the card number format and checks if the card number falls within a valid BIN range.
     */
    BASIC,

    /**
     * The gateway does not perform card verification.
     */
    NONE
}
