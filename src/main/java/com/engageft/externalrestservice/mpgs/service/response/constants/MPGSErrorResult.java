package com.engageft.externalrestservice.mpgs.service.response.constants;

public enum MPGSErrorResult {
    // The operation resulted in an error and hence cannot be processed
    ERROR
}
