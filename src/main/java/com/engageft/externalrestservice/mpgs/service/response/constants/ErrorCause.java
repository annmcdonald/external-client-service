package com.engageft.externalrestservice.mpgs.service.response.constants;

public enum ErrorCause {
    // The request was rejected because it did not conform to the API protocol.
    INVALID_REQUEST,

    // The request was rejected due to security reasons such as firewall rules,
    // expired certificate, etc.
    REQUEST_REJECTED,

    // The server did not have enough resources to process the request at the moment.
    SERVER_BUSY,

    // There was an internal system failure.
    SERVER_FAILED
}
