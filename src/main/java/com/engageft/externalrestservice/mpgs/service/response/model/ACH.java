package com.engageft.externalrestservice.mpgs.service.response.model;

public class ACH {
    private String accountType;
    private String bankAccountHolder;
    private String bankAccountNumber;
    private String routingNumber;
    private String secCode;
}
