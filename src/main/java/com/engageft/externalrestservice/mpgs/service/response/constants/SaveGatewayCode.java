package com.engageft.externalrestservice.mpgs.service.response.constants;

public enum SaveGatewayCode {
    BASIC_VERIFICATION_SUCCESSFUL, // The card number format was successfully verified and the card exists in a known range.
    EXTERNAL_VERIFICATION_BLOCKED, // The external verification was blocked due to risk rules.
    EXTERNAL_VERIFICATION_DECLINED, // The card details were sent for verification, but were was declined.
    EXTERNAL_VERIFICATION_DECLINED_EXPIRED_CARD, // The card details were sent for verification, but were declined as the card has expired.
    EXTERNAL_VERIFICATION_DECLINED_INVALID_CSC, // The card details were sent for verification, but were declined as the Card Security Code (CSC) was invalid.
    EXTERNAL_VERIFICATION_PROCESSING_ERROR, // There was an error processing the verification.
    EXTERNAL_VERIFICATION_SUCCESSFUL, // The card details were successfully verified.
    NO_VERIFICATION_PERFORMED // The card details were not verified.
}
