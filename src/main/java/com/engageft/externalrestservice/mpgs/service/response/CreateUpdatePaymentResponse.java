package com.engageft.externalrestservice.mpgs.service.response;

import com.engageft.externalrestservice.mpgs.service.response.constants.MPGSStatus;
import com.engageft.externalrestservice.mpgs.service.response.constants.SaveGatewayCode;
import com.engageft.externalrestservice.mpgs.service.response.constants.VerificationStrategy;

/**
 * The response from the gateway after creating or updating a payment.
 */
public class CreateUpdatePaymentResponse { 

    private MPGSStatus status;
    private String token;
    private SaveGatewayCode gatewayCode;
    private VerificationStrategy verificationStrategy;

    public CreateUpdatePaymentResponse() {
    }

    public MPGSStatus getStatus() {
        return status;
    }

    public String getToken() {
        return token;
    }

    public SaveGatewayCode getGatewayCode() {
        return gatewayCode;
    }

    public VerificationStrategy getVerificationStrategy() {
        return verificationStrategy;
    }
}
