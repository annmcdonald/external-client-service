package com.engageft.externalrestservice.constants;

public interface IErrorCode {

	Integer PAYVOX_SERVICE_ERROR_8000 = 8000;
	Integer PAYVOX_REQUEST_INVALID_8001 = 8001;

}
