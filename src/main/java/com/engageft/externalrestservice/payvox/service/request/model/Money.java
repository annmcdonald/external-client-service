package com.engageft.externalrestservice.payvox.service.request.model;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Money {

	@NotNull (message = "Amount cannot be null.")
	@Digits(fraction=2, integer = 10 )
	public Double  amount; // required - float 2 decimal
	
	@NotNull (message ="Currency cannont be null.")
	@Pattern(regexp = "USD", message = "Currency restricted to USD")
	public  String currency; // required - must be USD
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double  amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
