package com.engageft.externalrestservice.payvox.service.request.model.constants;

public enum RoleType {
	CUSTOMER,
	ADMIN
}
