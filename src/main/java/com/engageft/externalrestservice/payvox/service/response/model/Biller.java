package com.engageft.externalrestservice.payvox.service.response.model;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Biller {
	private String name;
	private String payeeId;
	private String nickname;
	private String category;
	private List<String> zipcode;
	private String accountMask;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<String> getZipcode() {
		return zipcode;
	}

	public void setZipcode(List<String> zipcode) {
		this.zipcode = zipcode;
	}

	public String getAccountMask() {
		return accountMask;
	}

	public void setAccountMask(String accountMask) {
		this.accountMask = accountMask;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
