package com.engageft.externalrestservice.payvox.service.request;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * This API is used to get a list of all categories.
i*
 * @author annmcdonald
 *
 */
public class BillerDirectoryCategoryRequest {
	
	private Long startResult;  // page number
	
	private Long maxResults;   // records per page
	
	public Long getStartResult() {
		return startResult;
	}

	public void setStartResult(Long startResult) {
		this.startResult = startResult;
	}

	public Long getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Long maxResults) {
		this.maxResults = maxResults;
	}

	@Override
	public
	String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}