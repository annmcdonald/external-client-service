package com.engageft.externalrestservice.payvox.service.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.engageft.externalrestservice.payvox.service.response.model.PayeeAccount;

public class QueryPayeeResponse {
    private String resultCode; // required - A000 is success, others are failures
    private String detailReason; // not required
    private List<PayeeAccount> payeeAccount;
	

	public String getResultCode() {
		return resultCode;
	}


	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}


	public String getDetailReason() {
		return detailReason;
	}


	public void setDetailReason(String detailReason) {
		this.detailReason = detailReason;
	}
	

	public List<PayeeAccount> getPayeeAccount() {
		return payeeAccount;
	}


	public void setPayeeAccount(List<PayeeAccount> payeeAccount) {
		this.payeeAccount = payeeAccount;
	}


	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
