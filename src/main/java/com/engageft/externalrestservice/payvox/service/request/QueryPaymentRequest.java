package com.engageft.externalrestservice.payvox.service.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 queryPayment is used to get all payments for a user

 * @author annmcdonald
 *
 */
public class QueryPaymentRequest {
	
	@NotNull( message = "Program id cannot be null." )
	@Size(min = 1, max = 16, message = "Program id size must be 1 and 16 characters." )
	String programId;
	
	@NotNull( message = "User id cannot be null." ) 
	String userId;  // 
	
	String payeeId; // returned when search Biller directory
	
	String accountId; //  cardholders identifier with the Biller 

	String nickname; //   
	String transactionId; // 
	String partnerTxnId;   //  32 char 
	String startDate;
	String endDate;
	Long startResult;  // page number
	Long maxResults; // max records per page
	
	
	public String getProgramId() {
		return programId;
	}


	public void setProgramId(String programId) {
		this.programId = programId;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getPayeeId() {
		return payeeId;
	}


	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}


	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public String getNickname() {
		return nickname;
	}


	public void setNickname(String nickname) {
		this.nickname = nickname;
	}


	public String getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	public String getPartnerTxnId() {
		return partnerTxnId;
	}


	public void setPartnerTxnId(String partnerTxnId) {
		this.partnerTxnId = partnerTxnId;
	}


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public Long getStartResult() {
		return startResult;
	}


	public void setStartResult(Long startResult) {
		this.startResult = startResult;
	}


	public Long getMaxResults() {
		return maxResults;
	}


	public void setMaxResults(Long maxResults) {
		this.maxResults = maxResults;
	}


	@Override
	public
	String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
