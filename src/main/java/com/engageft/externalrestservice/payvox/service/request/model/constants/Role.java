package com.engageft.externalrestservice.payvox.service.request.model.constants;

import java.util.ArrayList;
import java.util.List;

public enum Role {
	CARDHOLDER,
	ACCOUNTHOLDER,  
	CSR,
	ADMIN;
	
	List<String> adminRoles() {
		List<String> rList = new ArrayList();
		rList.add(this.ACCOUNTHOLDER.name());
		rList.add(this.CSR.name());
		rList.add(this.ADMIN.name());
		return rList;
	}
	
	List<String> profileRoles() {
		List<String> rList = new ArrayList();
		rList.add(this.CARDHOLDER.name());
		rList.add(this.CSR.name());
		return rList;
	}
}
