package com.engageft.externalrestservice.payvox.service.response.model.constants;

public enum ResultCode {
	//TODO - fill in error codes and messages
	 A000,  // API success
	 //F000-F009 - Authentication errors
	 F000,
	 //F100-F199 - Payment errors
	 
	 //F200-F299 - Errors related to validation, completeness and configuration rules
	 F220, // zero failure 
	 F237, // address missing for AVS enabled biller
	 F238, // cvv errors for CVV enabled biller
	 //F300-F399 - Errors originating from gateway
	 F300, // too low or high
	 F407,  // cxf xsd validation was setup properly you would not get this error
	
}
