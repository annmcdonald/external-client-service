package com.engageft.externalrestservice.payvox.service.response.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class PayeeAccount {
    private String payeeId; // required - 32 char - id of the Biller that will receive payment
    private String accountId; // required - card holder's identifier with the Biller
    private String nickname; // required - card holder's identifier with the Biller
	
	public String getPayeeId() {
		return payeeId;
	}


	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}


	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public String getNickname() {
		return nickname;
	}


	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
