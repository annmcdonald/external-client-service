package com.engageft.externalrestservice.payvox.service.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/*
 * Used for AddPayee, Delete Payee
 * 
 * @author amcdonald
 */
public class PayeeRequest {
	
	@NotNull( message = "Program id cannot be null." )
	@Size(min = 1, max = 16, message = "Program id size must be 1 and 16 characters." )
	String programId;  // provided by Aliaswire
	
	@NotNull( message = "User id cannot be null." ) 
	String userId;  // identifies the cardholder 
	
	@NotNull( message = "Zip Code cannot be null." ) 
	@Pattern ( regexp="^\\d{5}$|^\\d{5}-\\d{4}$", message="Zipcode must be 5 or 5 plus -4 digits.")
	String zipcode;  //  not used in delete payee request
	
	@NotNull( message = "PayeeId  cannot be null." ) 
	String payeeId;  // biller identifier - returned in search of Biller directory 
	
	@NotNull( message = "AccountId cannot be null." ) 
	String accountId; // the cardholder's identifier with the biller 
	
	@NotNull( message = "Nickname cannot be null." ) 
	String nickname; // the nickname the cardholder assigns to the payee 
	
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getPayeeId() {
		return payeeId;
	}
	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	@Override
	public
	String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
