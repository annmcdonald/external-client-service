package com.engageft.externalrestservice.payvox.service.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class QueryBillerDirectoryCategoryResponse {
	private String resultCode; // A000 is success, others are failures
	private String detailReason; // char 256
	private Long startResult; 
	private Boolean moreResults; 
	private List<String> name;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getDetailReason() {
		return detailReason;
	}

	public void setDetailReason(String detailReason) {
		this.detailReason = detailReason;
	}

	public Long getStartResult() {
		return startResult;
	}

	public void setStartResult(Long startResult) {
		this.startResult = startResult;
	}

	public Boolean getMoreResults() {
		return moreResults;
	}

	public void setMoreResults(Boolean moreResults) {
		this.moreResults = moreResults;
	}

	public List<String> getName() {
		return name;
	}

	public void setName(List<String> name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
