package com.engageft.externalrestservice.payvox.service.request;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.engageft.externalrestservice.aop.ValidateEnum;
import com.engageft.externalrestservice.aop.ValidateRole;
import com.engageft.externalrestservice.payvox.service.request.model.Address;
import com.engageft.externalrestservice.payvox.service.request.model.CreditCardAccount;
import com.engageft.externalrestservice.payvox.service.request.model.FundingAccount;
import com.engageft.externalrestservice.payvox.service.request.model.constants.Role;
import com.engageft.externalrestservice.payvox.service.request.model.constants.RoleType;
import com.engageft.externalrestservice.util.StringUtils;

/**

Profile
Each cardholder who wants to do bill payments must have a profile. 
Together with the profile PayVox stores a default Funding Account which is the Prepaid card that will be debited to fund the bill payments.
processProfile 

processProfile is used to create a new Profile and store the cardholder’s card, or update an existing profile and card.

 * 
 * @author annmcdonald
 *
 */
public class ProcessProfileRequest {

	@NotNull( message = "Program id cannot be null." )
	@Size(min = 1, max = 16, message = "Program id size must be 1 and 16 characters." )
	private String programId;  // provided by Aliaswire
	
	@NotNull( message = "User id cannot be null." ) 
	private String userId;  // unique identifier of cardholder 

	@ValidateRole(value = RoleType.CUSTOMER, message = "Role can only be CSR or CARDHOLDER.") 
	private Role role;  
	
	@Pattern(regexp = "EN|ES|en|es", message = "Locale restricted to EN or ES.")
	private String locale;  
	
	@NotNull( message = "First name cannot be null." )
	@Size(min = 1, max = 32, message = "First name size must be 1 and 32 characters." )
	private String fname;  // used to identify cardholder to Payee
	
	@NotNull( message = "Last name cannot be null." )
	@Size(min = 1, max = 32, message = "Last name size must be 1 and 32 characters." )
	private String lname; // used to identify cardholder to Payee 
	
	private String phone; // not required

	@Email(message = "Email should be valid.")
	@Size(min = 1, max = 64, message = "Email size must be 1 and 64 characters." )
	private String email;  // used to send alerts to the cardholder

	@Valid
	Address address; 
	
	@NotNull(message = "Funding Account cannot be null.")
	@Valid
	FundingAccount fundingAccount;  // The prepaid card that will be debited for the bill payments
	
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale.toUpperCase();
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public FundingAccount getFundingAccount() {
		return fundingAccount;
	}
	public void setFundingAccount(FundingAccount fundingAccount) {
		this.fundingAccount = fundingAccount;
	}
	
	@Override
	public
	String toString() {
		return StringUtils.indentToStringMultilineObjects(ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE));
	}
	
	public static void main(String[] args) {
		ProcessProfileRequest p = new ProcessProfileRequest();
		p.setProgramId("ProgramId");
		p.setUserId("UserId");
		p.setRole(Role.CSR);
		p.setLocale("US");
		p.setFname("first");
		p.setLname("Last");
		p.setPhone("7147133024");
		p.setEmail("ann.mcdonald@engageft.com");
		Address address = new Address("Main St.",null, "Livonia", "NY", "14450", "US"); 
		p.setAddress(address);
		CreditCardAccount ca = new CreditCardAccount("1111222233334444", "0520");
		ca.setNumber("12345");
		p.setFundingAccount(ca);
		System.out.println(p.toString());
	}
	
}
