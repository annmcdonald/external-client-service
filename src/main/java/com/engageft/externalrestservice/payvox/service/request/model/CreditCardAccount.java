package com.engageft.externalrestservice.payvox.service.request.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class CreditCardAccount extends FundingAccount {

	@NotNull (message = "Month cannot be null.")
	@Pattern ( regexp="^1[0-2]$|^0[1-9]$", message="Month must be in MM format.")
	private String expirationMonth; // required - formatted as MM

	@NotNull (message = "Year cannot be null.")
	@Pattern ( regexp="^2[0-9][0-9][0-9]$", message="Year must be in YYYY format.")
	private String expirationYear; // required - formatted as YYYY
	
	@Pattern ( regexp="^(\\d{3}|\\d{4})$", message="CCV number must be 3-4 digits.")
	private String cvv; // formated 3-4 digits - required if CVV check is enabled - returns F238 for cvv errors

    public CreditCardAccount(String expirationMonth, String expirationYear) {
		super();
		this.expirationMonth = expirationMonth;
		this.expirationYear = expirationYear;
	}
    
	public String getExpirationMonth() {
		return expirationMonth;
	}
	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}
	public String getExpirationYear() {
		return expirationYear;
	}
	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
