package com.engageft.externalrestservice.payvox.service.request.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class FundingAccount {
	
	@NotNull( message = "Number cannot be null." )
	@Size(min = 15, max = 32, message = "Number size must be 15 and 32 characters." )
	private String number;  // Card number at least 15 digits long and pass Mod10 check
	
	@Size(max = 64, message = "Name size must be less than  64 characters." )
	private String name;  // Full name of account holder 
	
	@Valid
	private Address address; // required if AVS is enabled for biller - returns F237 if missing for AVS enabled billers 
	
	@Size(max = 16, message = "Display size must be less than 16 characters." )
	private String display;   // masked account number as it should appear to end users - NOT the full account number
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
