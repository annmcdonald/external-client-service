package com.engageft.externalrestservice.payvox.service.request.model.constants;

public enum DeviceType {
	WEBSERVICE, 
	WEB, 
	IVR, 
	SMS, 
	MOBILE, 
	UNKNOWN
}
