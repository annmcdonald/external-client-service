package com.engageft.externalrestservice.payvox.service;


import com.engageft.externalrestservice.aop.ValidateRequestToExternalAPI;
import com.engageft.externalrestservice.client.ExternalRESTClient;
import com.engageft.externalrestservice.constants.IErrorCode;
import com.engageft.externalrestservice.exception.ExternalRESTServiceException;
import com.engageft.externalrestservice.payvox.service.request.BillerDirectoryRequest;
import com.engageft.externalrestservice.payvox.service.request.EvaluatePaymentRequest;
import com.engageft.externalrestservice.payvox.service.cache.BillerCache;
import com.engageft.externalrestservice.payvox.service.request.BillerDirectoryCategoryRequest;
import com.engageft.externalrestservice.payvox.service.request.PayeeRequest;
import com.engageft.externalrestservice.payvox.service.request.ProcessPaymentRequest;
import com.engageft.externalrestservice.payvox.service.request.ProcessProfileRequest;
import com.engageft.externalrestservice.payvox.service.request.QueryPayeeRequest;
import com.engageft.externalrestservice.payvox.service.request.QueryPaymentRequest;
import com.engageft.externalrestservice.payvox.service.response.AddPayeeResponse;
import com.engageft.externalrestservice.payvox.service.response.DeletePayeeResponse;
import com.engageft.externalrestservice.payvox.service.response.EvaluatePaymentResponse;
import com.engageft.externalrestservice.payvox.service.response.ProcessPaymentResponse;
import com.engageft.externalrestservice.payvox.service.response.ProcessProfileResponse;
import com.engageft.externalrestservice.payvox.service.response.QueryBillerDirectoryCategoryResponse;
import com.engageft.externalrestservice.payvox.service.response.QueryBillerDirectoryResponse;
import com.engageft.externalrestservice.payvox.service.response.QueryPayeeResponse;
import com.engageft.externalrestservice.payvox.service.response.QueryPaymentResponse;
import com.engageft.externalrestservice.payvox.service.response.model.Biller;
import com.engageft.externalrestservice.util.MarshallingUtil;
import com.engageft.externalrestservice.util.RESTUtil;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Set;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * The REST client to access External REST services.
 *
 */
/**
 * @author annmcdonald
 * @param <T>
 *
 */
@Service
public class PayVoxService<T> {
    private static final Logger LOG = LoggerFactory.getLogger(PayVoxService.class);
    
    @Autowired
    ExternalRESTClient externalRESTClient;
    
    @Autowired
    Validator externalRESTRequestValidator;
   
    @Autowired
    BillerCache billerCache;
    
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private @Value("${payvox.protocol}") String protocol;
    private @Value("${payvox.host}") String domain;
    private @Value("${payvox.path}") String path;
    private @Value("${payvox.basicauth.username}") String username;
    private @Value("${payvox.basicauth.password") String password;
    private @Value("${payvox.processprofile.resource}") String processProfileResource;
    private @Value("${payvox.processpayment.resource}") String processPaymentResource;
    private @Value("${payvox.evaluatepayment.resource}") String evaluatePaymentResource; 
    private @Value("${payvox.querypayment.resource}") String queryPaymentResource; 
    private @Value("${payvox.querybillerdirectorycategory.resource}") String queryBillerDirectoryCategoryResource; 
    private @Value("${payvox.querybillerdirectory.resource}") String queryBillerDirectoryResource; 
    private @Value("${payvox.addpayee.resource}") String addPayeeResource; 
    private @Value("${payvox.querypayee.resource}") String queryPayeeResource;  
    private @Value("${payvox.deletepayee.resource}") String deletePayeeResource;  
   
    /**
     * 
     * @param request
     * @param params
     * @return
     */
    @ValidateRequestToExternalAPI
    public  ProcessProfileResponse processProfile(ProcessProfileRequest request) throws ExternalRESTServiceException{
    	String url = protocol + domain + path + processProfileResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
		//
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, String.class, headers);
    		//
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 ProcessProfileResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), ProcessProfileResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
    }
    
    /**
     * 
     * @param request
     * @param params
     * @return
     */
    @ValidateRequestToExternalAPI
    public ProcessPaymentResponse processPayment(ProcessPaymentRequest request) {
    	String url = protocol + domain + path + processPaymentResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
		//
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, String.class, headers);
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 ProcessPaymentResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), ProcessPaymentResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
    } 
 
    /**
     * 
     * @param request
     * @param params
     * @return
     */
    @ValidateRequestToExternalAPI
    public  EvaluatePaymentResponse evaluatePayment(EvaluatePaymentRequest request) {
    	String url = protocol + domain + path + evaluatePaymentResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
		//
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, String.class, headers);
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 EvaluatePaymentResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), EvaluatePaymentResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
    } 

    /**
     * 
     * @param request
     * @param params
     * @return
     */
    @ValidateRequestToExternalAPI
    public QueryPaymentResponse queryPayment(QueryPaymentRequest request) {
    	String url = protocol + domain + path + queryPaymentResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
		//
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, String.class, headers);
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 QueryPaymentResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), QueryPaymentResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
    }  

    /**
     * 
     * @param request
     * @param params
     * @return
     */
    @ValidateRequestToExternalAPI
    public QueryBillerDirectoryResponse queryBillerDirectory(BillerDirectoryRequest request) {
    	String url = protocol + domain + path + queryBillerDirectoryResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, String.class, headers);
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 QueryBillerDirectoryResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), QueryBillerDirectoryResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
    } 
    
    /**
     * 
     * @param request
     * @param params
     * @return
     */
    public void populateBillerCache() {
    	//
    	// Clear cache
    	//
    	billerCache.initializeAllBillerMaps();
    	//
    	// Populate categorires
    	//
    	this.populateBillerCategories();
    	//
    	//
    	Set<String> categories = billerCache.getCategoryBillerIdMap().keySet();
    	BillerDirectoryRequest request = new BillerDirectoryRequest();
    	boolean more = true;
   		request.setMaxResults(100L);
   		request.setStartResult(0L);
    	String url = protocol + domain + path + queryBillerDirectoryResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
    	for(String category:categories) {
    		request.setCategory(category);
    		while(more) {
    			try {
    				ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.GET, request, String.class, headers);
    				// 
    				if (RESTUtil.isError(responseEntity.getStatusCode())) {
    					throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    				} else {
    					QueryBillerDirectoryResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), QueryBillerDirectoryResponse.class);
    					List<Biller> billerList = response.getBiller();
    					billerCache.addBillers(billerList);
    					request.setStartResult(response.getStartResult());
    					more = response.getMoreResults().booleanValue();
    				}
    			}catch (Exception e) {
    				throw new RuntimeException(e);
    			}
    		}
    	}
    }  
    
    /**
     * This API will only be called internally
     * 
     * @param request
     * @param params
     * @return
     */
    @ValidateRequestToExternalAPI
    public QueryBillerDirectoryCategoryResponse queryBillerDirectoryCategory(BillerDirectoryCategoryRequest request) {
    	String url = protocol + domain + path + queryBillerDirectoryCategoryResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
		//
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.GET, request, String.class, headers);
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 QueryBillerDirectoryCategoryResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), QueryBillerDirectoryCategoryResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
   
    }
    
    /**
     * 
     * @param request
     * @param params
     * @return
     */
    private void populateBillerCategories() {
    	String url = protocol + domain + path + queryBillerDirectoryCategoryResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
    	BillerDirectoryCategoryRequest request = new BillerDirectoryCategoryRequest();
    	QueryBillerDirectoryCategoryResponse response = new QueryBillerDirectoryCategoryResponse();
    	request.setStartResult(0L);
    	request.setMaxResults(100L);
    	boolean more = true;
    	while(more) {
    		//
    		try {
    			ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.GET, request, String.class, headers);
    			// 
    			if (RESTUtil.isError(responseEntity.getStatusCode())) {
    				throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    			} else {
    				response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), QueryBillerDirectoryCategoryResponse.class);
    				billerCache.addCategories(response.getName());
    				request.setStartResult(response.getStartResult());
    				more = response.getMoreResults().booleanValue();
    			}
    		}catch (Exception e) {
    			throw new RuntimeException(e);
    		}
    	}
    }  
    
    /**
     *  AddPayee 
     * @param request
     * @param params
     * @return AddPayeeResponse
     */
    @ValidateRequestToExternalAPI
    public  AddPayeeResponse addPayee(PayeeRequest request) throws ExternalRESTServiceException {
    	String url = protocol + domain + path + addPayeeResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
		//
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, String.class, headers);
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 AddPayeeResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), AddPayeeResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
    }  
    /**
     * 
     * @param request
     * @param params
     * @return
     */
    @ValidateRequestToExternalAPI
    public DeletePayeeResponse deletePayee(PayeeRequest request) {
    	String url = protocol + domain + path + deletePayeeResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
		//
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, String.class, headers);
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 DeletePayeeResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), DeletePayeeResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
    }  
    
    /**
     * 
     * @param request
     * @param params
     * @return
     */
    public QueryPayeeResponse queryPayee(QueryPayeeRequest request) {
    	String url = protocol + domain + path + queryPayeeResource;
    	//
    	//  Header formation
    	//
    	HttpHeaders headers = new HttpHeaders();
    	//String token = Base64Utils.encodeToString((this.username + ":" + this.password).getBytes(UTF_8));
		//headers.add("Authorization", "Basic " + token);
		//
    	try {
    		ResponseEntity<String> responseEntity = externalRESTClient.requestToExternalServer(url, HttpMethod.POST, request, String.class, headers);
    		// 
    		if (RESTUtil.isError(responseEntity.getStatusCode())) {
    		    throw new ExternalRESTServiceException(IErrorCode.PAYVOX_SERVICE_ERROR_8000, "[" + responseEntity.getStatusCodeValue() + "] " + responseEntity.getBody());
    		 } else {
    			 QueryPayeeResponse response = MarshallingUtil.convertJsonToPOJO(responseEntity.getBody(), QueryPayeeResponse.class);
    			 return response;
    		 }
    	}catch (Exception e) {
    		 throw new RuntimeException(e);
    	}
    }  
}
