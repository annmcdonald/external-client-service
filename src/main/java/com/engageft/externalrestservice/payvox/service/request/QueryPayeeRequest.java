package com.engageft.externalrestservice.payvox.service.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class QueryPayeeRequest {
	
	@NotNull( message = "Program Id cannot be null." )
	@Size(min = 1, max = 16, message = "Program id size must be 1 and 16 characters." )
	private String programId;  // provided by Aliaswire
	
	@NotNull( message = "User id  cannot be null." )
	private String userId;  // required 
	
	private Long startResult; // page number
	
	private Long maxResults; // max records per page
	
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getStartResult() {
		return startResult;
	}
	public void setStartResult(Long startResult) {
		this.startResult = startResult;
	}
	public Long getMaxResults() {
		return maxResults;
	}
	public void setMaxResults(Long maxResults) {
		this.maxResults = maxResults;
	}
	
	@Override
	public
	String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
