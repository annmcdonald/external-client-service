package com.engageft.externalrestservice.payvox.service.request.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Address {
	
	@Size(max = 64, message = "Street size must be less than  64 characters." )
	private String street;

	@Size(max = 32, message = "Line2 size must be less than  32 characters." )
	private String line2; 
	
	@Size(max = 32, message = "City size must be less than  32 characters." )
	private String city; 

	@Pattern ( regexp="/^(?:A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])*$/",
	message = "State code must be valid.")
	private String state; // 2 char -upper case

	@Pattern ( regexp="^\\d{5}$|^\\d{5}-\\d{4}$", message="Zipcode must be 5 or 5 plus -4 digits.")
	private String zipcode;

	@Size(min=2, max = 2, message = "Country code size must be 2 alpha characters. (ISO-3166)" )
	private String country; 
	
	public Address() {
		super();
	}
	
	public Address(String street, String line2, String city, String state, String zipcode, String country) {
		super();
		this.street = street;
		this.line2 = line2;
		this.city = city;
		this.state = state;
		this.zipcode = zipcode;
		this.country = country;
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
