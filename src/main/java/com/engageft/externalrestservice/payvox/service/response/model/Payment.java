package com.engageft.externalrestservice.payvox.service.response.model;

import java.sql.Timestamp;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.engageft.externalrestservice.payvox.service.request.model.Money;

public class Payment {
    private String transactionId; // not required - 32 char
    private Timestamp transactionDate; // required EST - yyyy-mm-ddThh:ss-sss-zz:zz
    private String status; // required  - 24 char  PROCESSED, PENDING, FAILIED, CANCELLED, REVERSAL_PROCESSED, REVERSAL_PENDING, REVERSAL_FAILED 
    private String approvalCode; //  not required - 32 char
    private Map<String, Object> extraData; // not required 
	private Money txnAmount; // not required
	private Money feeAmount; // not required
	private String processedAccountType; // not required - 16 char BANKACCOUNT, CREDITCARD, DEBITCARD, CONSUMERACCT, CLAIM
	private String processor; // not required  - 16 char  NYCE, TSYS, STAR, etc
	private String networkId; // not required - 16 char  nYCE, TSYS, STAR, etc
	private Timestamp clearanceDate; // not required
	

	public String getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	public Timestamp getTransactionDate() {
		return transactionDate;
	}


	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getApprovalCode() {
		return approvalCode;
	}


	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}


	public Map<String, Object> getExtraData() {
		return extraData;
	}


	public void setExtraData(Map<String, Object> extraData) {
		this.extraData = extraData;
	}


	public Money getTxnAmount() {
		return txnAmount;
	}


	public void setTxnAmount(Money txnAmount) {
		this.txnAmount = txnAmount;
	}


	public Money getFeeAmount() {
		return feeAmount;
	}


	public void setFeeAmount(Money feeAmount) {
		this.feeAmount = feeAmount;
	}


	public String getProcessedAccountType() {
		return processedAccountType;
	}


	public void setProcessedAccountType(String processedAccountType) {
		this.processedAccountType = processedAccountType;
	}


	public String getProcessor() {
		return processor;
	}


	public void setProcessor(String processor) {
		this.processor = processor;
	}


	public String getNetworkId() {
		return networkId;
	}


	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}


	public Timestamp getClearanceDate() {
		return clearanceDate;
	}


	public void setClearanceDate(Timestamp clearanceDate) {
		this.clearanceDate = clearanceDate;
	}
	

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
