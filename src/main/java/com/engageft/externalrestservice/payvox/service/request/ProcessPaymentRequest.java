package com.engageft.externalrestservice.payvox.service.request;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.engageft.externalrestservice.aop.ValidateRole;
import com.engageft.externalrestservice.payvox.service.request.model.FundingAccount;
import com.engageft.externalrestservice.payvox.service.request.model.Money;
import com.engageft.externalrestservice.payvox.service.request.model.constants.DeviceType;
import com.engageft.externalrestservice.payvox.service.request.model.constants.Role;
import com.engageft.externalrestservice.payvox.service.request.model.constants.RoleType;
import com.engageft.externalrestservice.util.StringUtils;

/**
 * API to execute payment transactions
 * 
 * @author amcdonald
 *
 */
public class ProcessPaymentRequest {
	
	@NotNull( message = "Program id cannot be null." )
	@Size(min = 1, max = 16, message = "Program id size must be 1 and 16 characters." )
	String programId;  // provided by Aliaswire

	@NotNull( message = "User id cannot be null." ) 
	String userId; // identifies cardholder to biller 
	
	@NotNull( message = "Payee id cannot be null." ) 
	String payeeId; //  returned when searching  Biller directory
	
	@NotNull( message = "Account id cannot be null." ) 
	String accountId; //  cardholder's  identifier with the Biller 
	
	String nickname; 
	
	@NotNull( message = "Transaction amount cannot be null." ) 
	@Valid
	Money txnAmount; 

	// PayVox will determine the fee and add it to the txnAmount during payment processing.  
    //PayVox will ignore any fee passed in by WebServices client
	@Valid
	Money feeAmount; 
	
	//boolean expedite // not supported
	
	//  Enumerated WEBSERVICE, WEB, IVR, SMS, MOBILE, UNKNOWN, default WEBSERVICE
	DeviceType deviceType = DeviceType.WEBSERVICE;
	
	String description;  // Reserved for future use

	@Size(min = 1, max = 32, message = "Partner transaction id size must be 1 and 32 characters." )
	String partnerTxnId;   // used for tracibility

	@Size(min = 1, max = 32, message = "Auth user id size must be 1 and 32 characters." )
	String authUserId;  // used for tracibility

	 @ValidateRole(value = RoleType.ADMIN, message = "Role can only be CSR, ADMIN, or ACCOUNTHOLDER.") 
	Role authUserRole; 
	 
	@Valid
	FundingAccount fundingAccount; //  not used
	
	Map<String, Object> extraData;
	
	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Money getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(Money txnAmount) {
		this.txnAmount = txnAmount;
	}

	public Money getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Money feeAmount) {
		this.feeAmount = feeAmount;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPartnerTxnId() {
		return partnerTxnId;
	}

	public void setPartnerTxnId(String partnerTxnId) {
		this.partnerTxnId = partnerTxnId;
	}

	public String getAuthUserId() {
		return authUserId;
	}

	public void setAuthUserId(String authUserId) {
		this.authUserId = authUserId;
	}

	public Role getAuthUserRole() {
		return authUserRole;
	}

	public void setAuthUserRole(Role authUserRole) {
		this.authUserRole = authUserRole;
	}

	public FundingAccount getFundingAccount() {
		return fundingAccount;
	}

	public void setFundingAccount(FundingAccount fundingAccount) {
		this.fundingAccount = fundingAccount;
	}

	public Map<String, Object> getExtraData() {
		return extraData;
	}

	public void setExtraData(Map<String, Object> extraData) {
		this.extraData = extraData;
	}

	@Override
	public
	String toString() {
		return StringUtils.indentToStringMultilineObjects(ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE));
	}
	
	
	
}
