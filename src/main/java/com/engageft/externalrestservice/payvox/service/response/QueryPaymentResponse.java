package com.engageft.externalrestservice.payvox.service.response;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import com.engageft.externalrestservice.payvox.service.response.model.Payment;

public class QueryPaymentResponse {
    private String resultCode; //A000 is success, others are failures
    private String detailReason;
    List<Payment> payment;
	private Integer startResult; 
	private Integer maxResults;
	

	public String getResultCode() {
		return resultCode;
	}


	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}


	public String getDetailReason() {
		return detailReason;
	}


	public void setDetailReason(String detailReason) {
		this.detailReason = detailReason;
	}

	public Integer getStartResult() {
		return startResult;
	}


	public void setStartResult(Integer startResult) {
		this.startResult = startResult;
	}


	public Integer getMaxResults() {
		return maxResults;
	}


	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}


	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
