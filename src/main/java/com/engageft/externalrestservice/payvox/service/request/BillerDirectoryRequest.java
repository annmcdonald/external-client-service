package com.engageft.externalrestservice.payvox.service.request;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * This API is used to get a list of all the billers.
 *  At least cateogry zipcode or name needs to be present 
 *  TODO - custom validation
 
 * @author annmcdonald
 *
 */
public class BillerDirectoryRequest {

	private Long startResult;   // page number

	private Long maxResults;  // max records per page 
	
	@Size(min=3, message = "Biller name size must be at least 3 characters." )
	private String name; 

	private String category; // Biller category as returned by category query

	@Pattern ( regexp="^\\d{5}$|^\\d{5}-\\d{4}$", message="Zipcode must be 5 or 5 plus -4 digits.")
	private String zipcode; 

	private String accountId; //  NOT supported yet	
	
	public Long getStartResult() {
		return startResult;
	}

	public void setStartResult(Long startResult) {
		this.startResult = startResult;
	}

	public Long getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Long maxResults) {
		this.maxResults = maxResults;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@Override
	public
	String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
