package com.engageft.externalrestservice.payvox.service.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.engageft.externalrestservice.payvox.service.response.model.Biller;

public class BillerCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(BillerCache.class);
	//Biller ID populated in the message. The maximum length is 10.
	//Example: 9998887771

	private Map<String, List<String>> categoryBillerIdMap = new HashMap<String,List<String>>();

	private Map<String, List<String>> zipcodeBillerIdMap = new HashMap<String,List<String>>();

	private Map<String, List<String>> nameBillerIdMap = new HashMap<String,List<String>>();
	
	private Map<String, Biller> billerMap = new HashMap<String, Biller>();
	
	private final static int categoryOnly = 1;
	private final static int nameOnly = 2;
	private final static int zipCodeOnly = 4;
	private final static int categoryNameOnly = 3;
	private final static int categoryZipCodeOnly = 5;
	private final static int nameZipCodeOnly = 6;
	private final static int categoryNameZipCode = 7;
	
	private int searchMask = 0;

	//
	//  The CategoryBillerId map has no delete feature. It stays insnyc by doing a complete
	// reinitialization
	//
	public void addCategory(String categoryName) {
		if(!categoryBillerIdMap.containsKey(categoryName)) {
			categoryBillerIdMap.put(categoryName, new ArrayList<String>());
		}
	}
	
	public void addCategories(List<String >categoryNameList) {
		for(String name: categoryNameList) {
			this.addCategory(name);
		}
	}
	
	
	
	public Map<String, List<String>> getCategoryBillerIdMap() {
		return categoryBillerIdMap;
	}

	public void setCategoryBillerIdMap(Map<String, List<String>> categoryBillerIdMap) {
		this.categoryBillerIdMap = categoryBillerIdMap;
	}

	public Map<String, List<String>> getZipcodeBillerIdMap() {
		return zipcodeBillerIdMap;
	}

	public void setZipcodeBillerIdMap(Map<String, List<String>> zipcodeBillerIdMap) {
		this.zipcodeBillerIdMap = zipcodeBillerIdMap;
	}

	public Map<String, List<String>> getNameBillerIdMap() {
		return nameBillerIdMap;
	}

	public void setNameBillerIdMap(Map<String, List<String>> nameBillerIdMap) {
		this.nameBillerIdMap = nameBillerIdMap;
	}

	public Map<String, Biller> getBillerMap() {
		return billerMap;
	}

	public void setBillerMap(Map<String, Biller> billerMap) {
		this.billerMap = billerMap;
	}

	public void initializeCategoryBillerIdMap() {
		LOG.info("Clearing categoryBillerIdMap.");
		categoryBillerIdMap.clear();
	}

	public void initializeZipcodeBillerIdMap() {
		LOG.info("Clearing zipcodeBillerIdMap.");
		zipcodeBillerIdMap.clear();
	}

	public void initializeNameBillerIdMap() {
		LOG.info("Clearing NameBillerIdMap.");
		nameBillerIdMap.clear();
	}

	public void initializeBillerMap() {
		LOG.info("Clearing BillerMap.");
		billerMap.clear();
	}

	public void initializeAllBillerMaps() {
		categoryBillerIdMap.clear();
		zipcodeBillerIdMap.clear();
		nameBillerIdMap.clear();
		billerMap.clear();
	}
	
	public List<Biller>  searchBiller(String category, String name, String zipCode){
		List<Biller> billerList = null;
		List<Biller> tmpBillerList = null;
		List<String> billerIdList = null;
		List<String> billerIdNameList = null;
  		if(!StringUtils.isEmpty(category)) searchMask+=categoryOnly;
		if(name != null && name.length() > 2) searchMask+=nameOnly;
		if(!StringUtils.isEmpty(zipCode)) searchMask+=zipCodeOnly;
		switch(searchMask) {
				case zipCodeOnly:
					billerIdList = this.zipcodeBillerIdMap.get(zipCode);
					billerList = createBillerListByIdList(billerIdList);
					break;
				case categoryOnly:
				case categoryZipCodeOnly:
					billerIdList = this.categoryBillerIdMap.get(category);
					billerList = createBillerListByIdList(billerIdList);
					if(searchMask == categoryZipCodeOnly && billerList != null && !billerList.isEmpty()) {
						tmpBillerList = billerList;
						billerList.clear();
						for(Biller biller: tmpBillerList) {
							if(biller.getZipcode().equals(zipCode)) {
								billerList.add(biller);
							}
						}
					}
					break;
				case nameOnly:
				case categoryNameOnly:
				case nameZipCodeOnly:
				case categoryNameZipCode:
					// get name list first
					billerIdList = this.nameBillerIdMap.get(name.substring(0, 2));
					if(billerIdList != null && !billerIdList.isEmpty()) {
						for(String n: billerIdList) {
							if(n.startsWith(name)) {
								if(billerIdNameList == null) {
									billerIdNameList = new ArrayList<String>();
								}
								billerIdNameList.add(n);
							}
						}
					}
					if(billerIdNameList != null) {
						billerIdList = billerIdNameList;
					}
					tmpBillerList = createBillerListByIdList(billerIdList);
					if(searchMask == nameOnly || tmpBillerList == null || tmpBillerList.isEmpty()) {
						billerList = tmpBillerList;
						break;
					}
					//
					// Second filter for name search (either category, zipcode or both)
					//
					for(Biller biller:tmpBillerList) {
						if(billerList == null) {
							billerList = new ArrayList<Biller> ();
						}
						if(searchMask ==categoryNameOnly || searchMask == categoryNameZipCode){
							if(biller.getCategory().equals(category)) {
								billerList.add(biller);
							}
						}else if(searchMask == nameZipCodeOnly ) {
							if(biller.getZipcode().equals(zipCode)) {
								billerList.add(biller);
							}
						}
					}
					//
					//  Third filter for name - zip code is placed in third position 
					//
					if(searchMask == categoryNameZipCode && billerList != null && !billerList.isEmpty()) {
						tmpBillerList = billerList;
						billerList.clear();
						for(Biller biller:tmpBillerList) {
							if(biller.getZipcode().equals(zipCode)) {
								billerList.add(biller);
							}
						}
					}
					break;
				 default:
					    break;
		}	
		return billerList;
	}
	
	private List<Biller> createBillerListByIdList(List<String> billerIdList){
		List<Biller> billerList = null;
		Biller biller = null;
		if(billerIdList != null && !billerIdList.isEmpty()) {
		for(String s: billerIdList) {
			    // initialize
				if(billerList == null && billerMap.get(s)!=null) {
					billerList = new ArrayList<Biller>();
				}
				biller = this.billerMap.get(s);
				if(biller != null) {
					billerList.add(biller);
				}else {
					LOG.warn("Biller with id: " + s + " is missing from cache.");
				}	
			}	
		}	
		return billerList;
	}
	
	public void addBillers(List<Biller> billerList) {
		for(Biller biller: billerList) {
			addBiller(biller);
		}
	}
	/**
	 * @param biller
	 */
	public void addBiller(Biller biller) {
		if(biller.getPayeeId() != null) {
			List<String> billerList;
			LOG.info("Adding biller data for billerId: " + biller.getPayeeId() + " to caches.");
			//
			// Add Biller to biller map with payeeId as the key
			//
			billerMap.put(biller.getPayeeId(), biller);
			//
			// Add Biller payeeId to List in the Category map
			//
			if(biller.getCategory() != null) {
				// This is a stop gap in case the category doesn't exist but it should exist
				// from an API that populates all the Categories that exist
				addCategory(biller.getCategory());
				//
			    billerList = categoryBillerIdMap.get(biller.getCategory());
				billerList.add(biller.getPayeeId());
			}
			//
			// Add Biller payeeId to List in the Name map
			//
			// The name of the biller is stored in a partitioned section per key that
			// is represented by the first three letters of the billers name
			//
			if(biller.getName() != null && biller.getName().length() > 2) {
				String firstThreeLetters = biller.getName().substring(0, 2);
				if(nameBillerIdMap.containsKey(firstThreeLetters)) {
					billerList = nameBillerIdMap.get(firstThreeLetters);
					billerList.add(biller.getPayeeId());
				}else {
					billerList = new ArrayList<String> ();
					billerList.add(biller.getPayeeId());
					nameBillerIdMap.put(firstThreeLetters, billerList);
				}
			}
			//
			// Add Biller payeeId to List in the ZipCode map
			//
			//  The biller can be associated with multiple zipcodes
			//
			if(biller.getZipcode() != null && !biller.getZipcode().isEmpty()) {
				List<String> zipcodeList = biller.getZipcode();
				//
				// Iterate through the zipcodes that the biller is associated and 
				// invert the relationship to exist as zipcode  being the key to a list of billers
				// in the same zipcode 
				//
				for(String zipCode: zipcodeList) {
					if(zipcodeBillerIdMap.containsKey(zipCode)) {
						billerList = zipcodeBillerIdMap.get(zipCode);
						billerList.add(biller.getPayeeId());
					}else {
						billerList = new ArrayList<String> ();
						billerList.add(biller.getPayeeId());
						zipcodeBillerIdMap.put(zipCode, billerList);
					}
				}
			}
		}
	}
}	
