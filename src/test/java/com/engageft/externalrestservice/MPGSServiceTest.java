package com.engageft.externalrestservice;


import com.engageft.externalrestservice.config.ExternalRESTServiceConfiguration;
import com.engageft.externalrestservice.mpgs.service.request.CreateUpdatePaymentRequest;
import com.engageft.externalrestservice.mpgs.service.request.params.TokenizationUrlParams;
import com.engageft.externalrestservice.mpgs.service.response.CreateUpdatePaymentResponse;
import com.engageft.externalrestservice.mpgs.service.response.InformationResponse;
import com.engageft.externalrestservice.mpgs.service.response.model.SourceOfFunds;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@ContextConfiguration(classes = {ExternalRESTServiceConfiguration.class})
//@TestPropertySource(locations = {"mpgstest.properties"})
@RunWith(SpringRunner.class)
public class MPGSServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(MPGSServiceTest.class);

    @Autowired
    com.engageft.externalrestservice.mpgs.service.MPGSService MPGSService;

    @Test
    public void getInformation() {
        //InformationResponse response = MPGSService.getInformation();
        //LOG.info("MPGS Information: " + response.toString());
        LOG.info("MPGS Information: skip");
    }
    
   // @Test
    public void storePayment() {
        CreateUpdatePaymentRequest request = new CreateUpdatePaymentRequest();
        TokenizationUrlParams params = new TokenizationUrlParams();
        params.setMerchantId("TEST123");
        SourceOfFunds sourceOfFunds = new SourceOfFunds();
        sourceOfFunds.setType("ACH");
        sourceOfFunds.setToken("ddd");
        request.setSourceOfFunds(sourceOfFunds);
        CreateUpdatePaymentResponse response = MPGSService.storePayment(request, params);
        LOG.info(response.toString());
    }

    // @Test
    public void updatePayment() {
        CreateUpdatePaymentRequest request = new CreateUpdatePaymentRequest();
        TokenizationUrlParams params = new TokenizationUrlParams();
        params.setMerchantId("TEST123");
        params.setTokenId("TEST111");
        CreateUpdatePaymentResponse response = MPGSService.updatePayment(request, params);
        LOG.info(response.toString());
    }

}
