package com.engageft.externalrestservice;

import com.engageft.externalrestservice.config.ExternalRESTServiceConfiguration;
import com.engageft.externalrestservice.exception.ExternalRESTServiceException;
import com.engageft.externalrestservice.payvox.service.request.model.FundingAccount;
import com.engageft.externalrestservice.payvox.service.request.PayeeRequest;
import com.engageft.externalrestservice.payvox.service.request.ProcessProfileRequest;
import com.engageft.externalrestservice.payvox.service.request.model.constants.Role;
import com.engageft.externalrestservice.payvox.service.response.AddPayeeResponse;
import com.engageft.externalrestservice.payvox.service.response.ProcessProfileResponse;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@ContextConfiguration(classes = {ExternalRESTServiceConfiguration.class})
@RunWith(SpringRunner.class)
@WebAppConfiguration
public class PayVoxServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(PayVoxServiceTest.class);

    @Autowired
    com.engageft.externalrestservice.payvox.service.PayVoxService payvoxService;
    
    @Test
    public void addProfileValidationTest() {
    	LOG.info("Process Profile Validation test begin.");
       ProcessProfileRequest profile = new ProcessProfileRequest();
       FundingAccount fundingAccount = new FundingAccount();
      // fundingAccount.setNumber("123456");
       fundingAccount.setName("name");
       profile.setEmail("ann.mcdonald@engageft.com");
       profile.setFname("Ann");
       profile.setLname("McDonald");
       profile.setPhone("7147133024");;
       profile.setProgramId("programId");;
       profile.setUserId("userId");;
       //profile.setLocale("VEN");
       profile.setRole(Role.CARDHOLDER);
       profile.setFundingAccount(fundingAccount);
       ProcessProfileResponse response = null;
       try {
    	   response = payvoxService.processProfile(profile);
       }catch (ExternalRESTServiceException e) {
    	  System.out.println("In exception for rest call."); 
    	  System.out.println(e.getMessage());
       }catch (Exception e) {
    	   System.out.println("you are in exception.");
       }
    	LOG.info("Process Profile Validation test end.");
    }

   // @Test
    public void addPayeeValidationTest() {
       PayeeRequest payee = new PayeeRequest();
       payee.setAccountId("accountId");
       payee.setNickname("nickname");
       payee.setPayeeId("payeeId");
       payee.setZipcode("zipcode");
       payee.setProgramId("programId");
       payee.setUserId("userId");
       AddPayeeResponse response = null;
       try {
    	   response = payvoxService.addPayee(payee);
       }catch (ExternalRESTServiceException e) {
    	   response = null;
       }
       //assertNotNull(response);
       payee.setProgramId(null);
       payee.setUserId(null);
       try {
    	   response = payvoxService.addPayee(payee);
       }catch (ExternalRESTServiceException e) {
    	   assertNotNull(e.getMessage());
    	   assertTrue(e.getMessage().toLowerCase().contains("payee"));
       }
       assertNull(response);
    }
}
